﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;

namespace TesteGeradorPDF.Service
{
    public class GeradorGabarito
    {
        private Document doc;
        PdfWriter writer;
        PdfContentByte cb;
        float heightRectangle;

        public GeradorGabarito(string path)
        {
            doc = new Document(PageSize.A4);//criando e estipulando o tipo da folha usada
            doc.SetMargins(15, 15, 15, 15);//estibulando o espaçamento das margens que queremos
            doc.AddCreationDate();//adicionando as configuracoes

            //caminho onde sera criado o pdf + nome desejado
            //OBS: o nome sempre deve ser terminado com .pdf
            string caminho = path;//@"C:\Users\anton\Desktop\Projetos\projeto Borsari&Borsari\x\Relatorios\" + "CONTRATO.pdf";

            //criando o arquivo pdf embranco, passando como parametro a variavel                
            //doc criada acima e a variavel caminho 
            //tambem criada acima.
            writer = PdfWriter.GetInstance(doc, new FileStream(caminho, FileMode.Create));
           
        }

        public async Task Gerar()
        {

            doc.Open();
            doc.NewPage();
            cb = writer.DirectContent;
            await GerarRetanguloInfoCandidato();
            await GerarInfoCandidato();
            await GerarLogo();
            await GerarTurma();
            await GerarRetanguloExemploPreenchimento();
            await GerarExemploPreenchimento();
            await GerarTriandulosReferencia();

            doc.Close();

        }

        private async Task GerarTriandulosReferencia()
        {
            cb.MoveTo(250f, 350f);
            cb.LineTo(265f, 350f);
            cb.LineTo(250f, 365f);
            
            cb.SetColorFill(BaseColor.BLACK);
            cb.Fill();
            cb.ClosePathStroke();
        }

        private async Task GerarExemploPreenchimento()
        {
            string text = "Exemplo de preenchimento";
            var baseFont = BaseFont.CreateFont();
            var font = new Font(baseFont, 22);
            font.SetStyle("bold");
            await GerarParagrafo(text, font);

            await GerarParagrafo("    Preenchimento correto:           Preenchimento errado:");

            var bottomMargin = PageSize.A4.Height - doc.TopMargin - heightRectangle - 36.5f - 5;
            cb.Circle(175f, bottomMargin, 8);
            cb.Fill();

            var img = System.Drawing.Image.FromFile(@"C:\Users\jonas\Pictures\untitled2.png");
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Jpeg);
                iTextSharp.text.Image imgTxt = new Jpeg(ms.ToArray());
                var heigthLogo = 18.5f;
                var widthLogo = 70f;                
                
                var leftMargim = 325f;
                //cb.AddImage(imgTxt, img.Width / 2, 0, 0, img.Height / 2, 36.0f, bottomMargim);
                cb.AddImage(imgTxt, widthLogo, 0, 0, heigthLogo, leftMargim, bottomMargin - 10);
            }
        }

        private async Task GerarRetanguloExemploPreenchimento()
        {
            var exemploheightRectangle = 50f;
            var widthRectangle = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
            var bottomMargin = PageSize.A4.Height - doc.TopMargin - heightRectangle - exemploheightRectangle - 5;
            cb.Rectangle(doc.LeftMargin, bottomMargin, widthRectangle, exemploheightRectangle);
            cb.Stroke();
        }

        private async Task GerarTurma()
        {
            float turmaHeigthRectangle = 80f;
            var turmaWidthRectangle = 80f;
            var bottomMargin = PageSize.A4.Height - doc.TopMargin - heightRectangle + 5;
            var leftMargim = PageSize.A4.Width - doc.RightMargin - turmaWidthRectangle - 5;
            cb.Rectangle(leftMargim, bottomMargin, turmaWidthRectangle, turmaHeigthRectangle);            
            cb.Stroke();
            
            cb.SetFontAndSize(BaseFont.CreateFont(), 15);
            cb.BeginText();
            cb.MoveText(leftMargim + 10, bottomMargin + 65);
            cb.ShowText("TURMA");
            cb.EndText();
        }

        private async Task GerarInfoCandidato()
        {
            //ColumnText ct = new ColumnText(cb);

            //var widthLogo = (PageSize.A4.Width - doc.LeftMargin - doc.RightMargin) / 3;
            //ct.SetSimpleColumn( = (widthLogo * 2) + doc.LeftMargin - 5;

            string cliente = "Prefeitura Municipal de Votorantim";
            await GerarParagrafo(cliente);

            string concurso = "Processo Seletivo - PEB II - Educação Física - Nº 002/2018)";
            await GerarParagrafo(concurso);

            string cargo = "Edificações";
            await GerarParagrafo(cargo);

            await GerarParagrafo(System.Environment.NewLine);

            await GerarParagrafo("E.E. Profº Wilson Prestes Miramontes - 14h00");

            await GerarParagrafo("Sala: 1 - Data: 11/11/2018");

            await GerarParagrafo(System.Environment.NewLine);

            await GerarParagrafo("ADEMIR PORTILLO JUNIOR");

            await GerarParagrafo("Nascimento: 30/07/1980");

            await GerarParagrafo("RG: 320002858");

            await GerarParagrafo("Inscrição: 2000369566");
            await GerarParagrafo("Controle: 1");
        }

        private async Task<Paragraph> GerarParagrafo(string text, Font font = null)
        {
            Paragraph pText = new Paragraph(text);
            if (font != null)
                pText.Font = font;
            pText.Alignment = Element.ALIGN_JUSTIFIED;
            pText.ExtraParagraphSpace = 5;
            pText.FirstLineIndent = 5;
            pText.IndentationLeft = 5;

            doc.Add(pText);
            return pText;
        }

        private async Task GerarLogo()
        {
            var img = System.Drawing.Image.FromFile(@"C:\Users\jonas\Pictures\untitled.png");
            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Jpeg);
                iTextSharp.text.Image imgTxt = new Jpeg(ms.ToArray());
                var heigthLogo = 80f;
                var widthLogo = (PageSize.A4.Width -doc.LeftMargin - doc.RightMargin) / 3;
                var topMargin = doc.TopMargin + 2;
                var bottomMargin = PageSize.A4.Height - topMargin - heigthLogo;
                var leftMargin = (widthLogo * 2) + doc.LeftMargin - 5;
                //cb.AddImage(imgTxt, img.Width / 2, 0, 0, img.Height / 2, 36.0f, bottomMargim);
                cb.AddImage(imgTxt, widthLogo, 0, 0, heigthLogo, leftMargin, bottomMargin);
            }
        }

        private async Task GerarRetanguloInfoCandidato(int ajusteHeigth = 0)
        {            
            heightRectangle = ((PageSize.A4.Height - doc.BottomMargin) / 4) + 15 + ajusteHeigth;
            var widthRectangle = PageSize.A4.Width - doc.LeftMargin - doc.RightMargin;
            var bottomMargin = PageSize.A4.Height - doc.TopMargin - heightRectangle;

            cb.Rectangle(doc.LeftMargin, bottomMargin, widthRectangle, heightRectangle);
          
            cb.Stroke();
        }
    }
}
